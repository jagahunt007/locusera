<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<section id="ext_menu-b">


		 
		 <nav class="navbar  navbar-default navbar-fixed-top" style="margin-bottom: 0px;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
     <a class="navbar-brand " href="index.html" style="color:#080808;"><p class="logo_txt">LOCUSERA SOLUTIONS PVT LTD</p><img class="logo" src="assets/images/logo_cut.png"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
		<li><a href="about.htm">About</a></li>
        <li><a href="services.htm">Services</a></li>
        <li >
          <a href="jobseeker.html">Job seeker</a>
          
        </li>
        <li><a href="enquiry.htm">Business Enquiry</a></li>
		   <li><a href="blog.htm">Our Blog</a></li>
        <li><a href="contactus.htm">Contact</a></li>
      </ul>
    <!--   <ul class="nav navbar-nav navbar-right">
         <li><a href="join.html"><span class="glyphicon glyphicon-user"></span> Sign Up/Login</a></li>
        
      </ul> -->
    </div>
  </div>
</nav>
		<p id="date" style="color: white;
    text-align: center;
    background-color: cadetblue;margin-top:65px"></p>  
		

</section>

<section class="mbr-slider mbr-section mbr-section__container carousel slide mbr-section-nopadding mbr-after-navbar" data-ride="carousel" data-keyboard="false" data-wrap="true" data-pause="false" data-interval="5000" id="slider-d">
    <div>
        <div>
            <div>
                <ol class="carousel-indicators">
                    <li data-app-prevent-settings="" data-target="#slider-d" data-slide-to="0" class="active"></li><li data-app-prevent-settings="" data-target="#slider-d" class="" data-slide-to="1"></li><li data-app-prevent-settings="" data-target="#slider-d" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox" style="height:500px;">
                    <div class="mbr-section mbr-section-hero carousel-item dark center mbr-section-full active" data-bg-video-slide="false" style="background-image: url(assets/images/full-unsplash-photo-1476038270590-87bf4cb16a90-2000x1330-24.jpg);">
                        <div class="mbr-table-cell">
                            <div class="mbr-overlay"></div>
                            <div class="container-slide container">
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2 text-xs-center" style="border-radius:3px;">
                                        <h2 class="mbr-section-title " style="font-size: 3.2rem;"></h2>
                                        <p class="mbr-section-lead lead"></p>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><div class="mbr-section mbr-section-hero carousel-item dark center mbr-section-full" data-bg-video-slide="false" style="background-image: url(assets/images/slider_2.jpg);">
                        <div class="mbr-table-cell">
                            <div class="mbr-overlay"></div>
                            <div class="container-slide container" style="float:left">
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2 text-xs-center" style="border-radius:3px;">
                                        <h2 class="mbr-section-title " style="font-size: 3.2rem;"></h2>
                                        <p class="mbr-section-lead lead"></p>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><div class="mbr-section mbr-section-hero carousel-item dark center mbr-section-full" data-bg-video-slide="false" style="background-image: url(assets/images/full-unsplash-photo-1474403078171-7f199e9d1335-2000x1333-45.jpg);">
                        <div class="mbr-table-cell">
                            <div class="mbr-overlay"></div>
                            <div class="container-slide container" style="float:right">
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2 text-xs-center" style="border-radius:3px;">
                                        <h2 class="mbr-section-title display-1" style="font-size: 3.2rem;"></h2>
                                        <p class="mbr-section-lead lead"></p>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <a data-app-prevent-settings="" class="left carousel-control" role="button" data-slide="prev" href="#slider-d">
                    <span class="icon-prev" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a data-app-prevent-settings="" class="right carousel-control" role="button" data-slide="next" href="#slider-d">
                    <span class="icon-next" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
				
            </div>
        </div>
    </div>
	<div class="col-md-12" style="width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;">
<section class="mbr-section" id="form2-e" style="rgba(46, 46, 46, 0);padding-top: 150px; padding-bottom: 120px;">
        <div class="mbr-section mbr-section__container mbr-section__container--middle">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-xs-center">
                    <h3 class="mbr-section-title display-2">&nbsp;Send Your Resume</h3>
                    <small class="mbr-section-subtitle"></small>
                </div>
            </div>
        </div>
    </div>
    <div class="mbr-section mbr-section-nopadding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-10 col-lg-offset-1" data-form-type="formoid">
                   
                    <form:form class="mbr-form" action="submitResume.htm" method="post" data-form-title="&amp;nbsp;Search Job" enctype="multipart/form-data" commandName="resumeCommand">
                       
                        <div class="mbr-subscribe mbr-subscribe-dark input-group">
                            <form:input type="file" class="form-control" path="resume" required="required" ></form:input>
                            <span class="input-group-btn"><input type="submit" class="btn btn-primary"/>Send</span>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
	
</section>
</div>
</section>

		<hr>
		<h3 style="padding: 40px;color: cadetblue;text-align:center;">Our Services</h3>
<div class="container">
<section class="mbr-cards mbr-section mbr-section-nopadding" id="features6-1r" style="background-color: rgb(239, 239, 239);">

    

    <div class="mbr-cards-row row">
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
              <div class="card cart-block">
                  <div class="card-img"><a href="services.html" class="mbri-idea mbr-iconfont mbr-iconfont-features3"></a></div>
                  <div class="card-block">
                    <h4 class="card-title" style="color:#115e61">Recruitment / Talent Acquisition</h4>
                    
                    <p class="card-text"></p>
                    <div class="card-btn"><a href="services.html" class="btn btn-primary">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;background-color: #99a6a7;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><a href="services.html" class="etl-icon icon-recycle mbr-iconfont mbr-iconfont-features3"></a></div>
                    <div class="card-block">
                        <h4 class="card-title" style="color:#115e61">Training And Development</h4>
                        
                        <p class="card-text"></p>
                        <div class="card-btn"><a href="services.html" class="btn btn-primary">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><a href="services.html" class="mbri-briefcase mbr-iconfont mbr-iconfont-features3"></a></div>
                    <div class="card-block">
                        <h4 class="card-title" style="color:#115e61">Compliance Management</h4>
                        
                        <p class="card-text"></p>
                        <div class="card-btn"><a href="services.html" class="btn btn-primary">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;background-color: #99a6a7;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><a href="services.html" class="mbri-edit mbr-iconfont mbr-iconfont-features3"></a></div>
                    <div class="card-block">
                        <h4 class="card-title" style="color:#115e61">HR &amp; Legal Audits</h4>
                        
                        <p class="card-text"></p>
                        <div class="card-btn"><a href="services.html" class="btn btn-primary">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</section>
<section class="mbr-cards mbr-section mbr-section-nopadding" id="features6-1s" style="background-color: rgb(239, 239, 239);">

    

    <div class="mbr-cards-row row">
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;background-color: #99a6a7;">
            <div class="container">
              <div class="card cart-block">
                  <div class="card-img"><a href="services.html" class="mbri-users mbr-iconfont mbr-iconfont-features3"></a></div>
                  <div class="card-block">
                    <h4 class="card-title" style="color:#115e61">Out Sourcing &amp; Permanent Staffing</h4>
                    
                    <p class="card-text hidden animated"></p>
                    <div class="card-btn"><a href="services.html" class="btn btn-primary hidden animated">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><a href="services.html" class="mbri-cash mbr-iconfont mbr-iconfont-features3"></a></div>
                    <div class="card-block">
                        <h4 class="card-title" style="color:#115e61">Payroll management</h4>
                        
                        <p class="card-text hidden animated"></p>
                        <div class="card-btn"><a href="services.html" class="btn btn-primary hidden animated">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;background-color: #99a6a7;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><a href="services.html" class="mbri-laptop mbr-iconfont mbr-iconfont-features3"></a></div>
                    <div class="card-block">
                        <h4 class="card-title" style="color:#115e61">Green Field Project Execution</h4>
                        
                        <p class="card-text hidden animated"></p>
                        <div class="card-btn"><a href="services.html" class="btn btn-primary hidden animated">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><a href="services.html" class="etl-icon icon-global mbr-iconfont mbr-iconfont-features3"></a></div>
                    <div class="card-block">
                        <h4 class="card-title" style="color:#115e61">Patents, IPR, Trademark &amp; Copyrights filing</h4>
                        
                        <p class="card-text hidden animated"></p>
                        <div class="card-btn"><a href="services.html" class="btn btn-primary hidden animated">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</section>

		</div>
		<hr>
	<div class="container">
		<div class="row">
		<h3 style="padding: 40px;color: cadetblue;text-align:center;">UPDATES ABOUT LOCUSERA.</h3>
			<div class="col-md-6">
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<span class="glyphicon glyphicon-list-alt"></span><b>Events</b></div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12">
								<ul class="demo1">
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												<td><img src="assets/images/img-2.png" width="60" class="img-circle" /></td>
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												<td><img src="assets/images/img-2.png" width="60" class="img-circle" /></td>
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												<td><img src="assets/images/img-2.png" width="60" class="img-circle" /></td>
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												<td><img src="assets/images/img-1.png" width="60" class="img-circle" /></td>
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												<td><img src="assets/images/img-1.png" width="60" class="img-circle" /></td>
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												<td><img src="assets/images/img-2.png" width="60" class="img-circle" /></td>
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												<td><img src="assets/images/img-2.png" width="60" class="img-circle" /></td>
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel-footer">

					</div>
				</div>
			</div>
			
		<div class="col-md-6">
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<span class="glyphicon glyphicon-list-alt"></span><b>News</b></div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12">
								<ul class="demo2">
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
									<li class="news-item">
										<table cellpadding="4">
											<tr>
												
												<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in venenatis enim... <a href="#">Read more...</a></td>
											</tr>
										</table>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel-footer">

					</div>
				</div>
			</div>
					
			</div>
			

		</div>

<hr>
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
        <h3 style="padding: 40px 0px;color: cadetblue;text-align:center;">TESTIMONIALS</h3>
            <div id="testimonial-slider" class="owl-carousel">
                <div class="testimonial">
                    <div class="pic">
                        <img src="assets/images/img-1.png" alt="">
                    </div>
                    <div class="testimonial-content">
                        <p class="description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed accumsan diam. Suspendisse molestie nibh at tempor mollis. Integer aliquet facilisis felis, ac porta est cursus et. Vestibulum sollicitudin nisl ut urna egestas faucibus. Nullam pharetra, nisl eu feugiat.
                        </p>
                        <h3 class="testimonial-title">williamson</h3>
                        <small class="post">/ Web Designer</small>
                    </div>
                </div>
 
                <div class="testimonial">
                    <div class="pic">
                        <img src="assets/images/img-2.png" alt="">
                    </div>
                    <div class="testimonial-content">
                        <p class="description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed accumsan diam. Suspendisse molestie nibh at tempor mollis. Integer aliquet facilisis felis, ac porta est cursus et. Vestibulum sollicitudin nisl ut urna egestas faucibus. Nullam pharetra, nisl eu feugiat.
                        </p>
                        <h3 class="testimonial-title" style="color:#c0a375;">kristiana</h3>
                        <small class="post">/ Web Developer</small>
                    </div>
                </div>

                   <div class="testimonial">
                    <div class="pic">
                        <img src="assets/images/img-2.png" alt="">
                    </div>
                    <div class="testimonial-content">
                        <p class="description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed accumsan diam. Suspendisse molestie nibh at tempor mollis. Integer aliquet facilisis felis, ac porta est cursus et. Vestibulum sollicitudin nisl ut urna egestas faucibus. Nullam pharetra, nisl eu feugiat.
                        </p>
                        <h3 class="testimonial-title" style="color:#c0a375;">nickson</h3>
                        <small class="post">/ Web Developer</small>
                    </div>
                </div>

                   <div class="testimonial">
                    <div class="pic">
                        <img src="assets/images/img-1.png" alt="">
                    </div>
                    <div class="testimonial-content">
                        <p class="description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed accumsan diam. Suspendisse molestie nibh at tempor mollis. Integer aliquet facilisis felis, ac porta est cursus et. Vestibulum sollicitudin nisl ut urna egestas faucibus. Nullam pharetra, nisl eu feugiat.
                        </p>
                        <h3 class="testimonial-title" style="color:#c0a375;">gullabro</h3>
                        <small class="post">/ Web Developer</small>
                    </div>
                </div>


                   <div class="testimonial">
                    <div class="pic">
                        <img src="assets/images/img-2.png" alt="">
                    </div>
                    <div class="testimonial-content">
                        <p class="description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed accumsan diam. Suspendisse molestie nibh at tempor mollis. Integer aliquet facilisis felis, ac porta est cursus et. Vestibulum sollicitudin nisl ut urna egestas faucibus. Nullam pharetra, nisl eu feugiat.
                        </p>
                        <h3 class="testimonial-title" style="color:#c0a375;">ranush</h3>
                        <small class="post">/ Web Developer</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<section class="mbr-section mbr-section-hero mbr-section-full header2 mbr-parallax-background" id="header2-i" style="background-image: url(assets/images/landscape.jpg);">

    <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(0, 0, 0);">
    </div>

    <div class="mbr-table mbr-table-full">
        <div class="mbr-table-cell">

            <div class="container">
                <div class="mbr-section row">
                    <div class="mbr-table-md-up">
                        
                        
                        

                        <div class="mbr-table-cell col-md-5 content-size text-xs-center text-md-right">

                            <h3 class="mbr-section-title display-2">Lorem Ipsum</h3>

                            <div class="mbr-section-text lead">
                                <p>Lorem Ipsum</p>
                            </div>

                            <div class="mbr-section-btn"><a class="btn btn-primary" href="https://mobirise.com">MORE</a></div>

                        </div>
                        <div class="mbr-table-cell mbr-valign-top mbr-left-padding-md-up col-md-7 image-size" style="width: 70%;">
                            <div class="mbr-figure"><iframe class="mbr-embedded-video" src="https://www.youtube.com/embed/NBJjMQ1eF2I" width="1280" height="720" frameborder="0" allowfullscreen></iframe></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="mbr-arrow mbr-arrow-floating hidden-sm-down" aria-hidden="true"><a href="#gallery3-k"><i class="mbr-arrow-icon"></i></a></div>

</section>
