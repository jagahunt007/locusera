<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<section class="mbr-section article mbr-parallax-background mbr-after-navbar" id="msg-box8-y" style="background-image: url(assets/images/full-unsplash-photo-1459499362902-55a20553e082-2000x1335-50.jpg); padding-top: 120px; padding-bottom: 120px;">

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(34, 34, 34);">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-xs-center">
                <h3 class="mbr-section-title display-2" style="color:white;">About us</h3>
                <div class="lead"></div>
                
            </div>
        </div>
    </div>

</section>
<section class="mbr-cards mbr-section mbr-section-nopadding" id="features4-18" style="background-color: rgb(255, 255, 255);">

    

    <div class="mbr-cards-row row">
        <div class="mbr-cards-col col-xs-12 col-lg-4" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox"><a href="https://mobirise.com" class="mbri-briefcase mbr-iconfont mbr-iconfont-features4" style="color: black;"></a></div>
                    <div class="card-block">
                        <h4 class="card-title" style="color:cadetblue">Company Profile </h4>
                      
                        <p class="card-text">LOCUSERA Solutions Pvt. Ltd. is an organization that provide innovative and strategic path in making Human Resource more customized to the needs of various Industries. Nevertheless, focusing also on the career prospects of the personnel and has been recognized as the most resourceful entity in consulting services Pan India. We work as a collaborative, caring partner to ensure the efficient delivery of HR Services in meeting the needs of those we serve.. and we are committed in demonstrating integrity and a positive forward thinking approach with the services we offer. Providing proactive, innovative services by removing barriers and enhancing the quality of work life for those we serve.</p>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-4" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox"><a href="https://mobirise.com" class="mbri-user mbr-iconfont mbr-iconfont-features4" style="color: black;"></a></div>
                    <div class="card-block">
                        <h4 class="card-title" style="color:cadetblue">Our Strategy</h4>
                       
                        <p class="card-text">At Locusera Solutions, we believe quality is not just another goal, it is our basic strategy for continuous growth. With strong focus on quality, trust and demonstrate strong commitment towards process excellence resulting in enhanced productivity with the best professionals and effective implementation of best practices at all levels.</p>
                        
                    </div>
                </div>
          </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-4" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox"><a href="https://mobirise.com" class="mbri-question mbr-iconfont mbr-iconfont-features4" style="color: black;"></a></div>
                    <div class="card-block">
                        <h4 class="card-title" style="color:cadetblue">Why LOCUSERA ?</h4>
                      
                        <p class="card-text">We have the flexibility to deliver services quickly and cost-effectively. Our confidence in our ability to think ahead makes us willing to be measured against any business outcomes. Which means that with Locusera solutions, you get the advantage of acquiring quality services along with the advantage of being taken the best professionals within the stipulated time.
</p>
                        
                    </div>
                </div>
            </div>
        </div>
        
        
        
    </div>
</section>
<section class="mbr-section mbr-section__container" id="buttons1-17" style="background-color: rgb(255, 255, 255); padding-top: 20px; padding-bottom: 20px;">

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-xs-center"><a class="btn btn-primary" href="#" data-toggle="modal" data-target="#vission">VISION</a> 
				
				<a class="btn btn-black btn-black-outline" href="#" data-toggle="modal" data-target="#commitment">COMMITMENT</a> 
				<a class="btn btn-primary" href="#" data-toggle="modal" data-target="#mission">MISSION</a></div>
            </div>
        </div>
    </div>

</section>
 <!-- Modal 1-->
  <div class="modal fade" id="vission" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color:cadetblue">Vission</h4>
        </div>
        <div class="modal-body">
          <p>To provide HR Solutions of International standards through value added services

To establish our company amongst the Best HR Solutions players in all departments of our core competency.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
 
   <!-- Modal 2-->
  <div class="modal fade" id="mission" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color:cadetblue">Mission</h4>
        </div>
        <div class="modal-body">
          <p>To provide innovative, professional and personalized services to the Corporate World through HR Solutions.

To maintain Quality Management System as per ISO guidelines with an objective to achieve the descried quality in every activity.

To practice quality improvement process and achieve a significant improvement in business results.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
   <!-- Modal 3-->
  <div class="modal fade" id="commitment" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color:cadetblue">Commitment</h4>
        </div>
        <div class="modal-body">
          <p>We at LOCUSERA are committed to provide high-end services to our esteemed clients in terms of Recruitments, Staffing Solutions, Payroll Management, Compliance Management, HR Audits, Green Field Project Execution, Patent Filings (National & International), Trademark & Copyrights Filings.

We focus our commitment by contributing to our society in terms of providing HR Solutions to potential Clients in the market, generating awareness and providing customized solutions to corporate Houses.

We always ensure to follow all cardinal rules and regulations of the Indian Constitution, State Legislatures including Laws of the Land imposed by the State and Central Govt. as applicable.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
    <input name="animation" type="hidden">