 <div id="demo">
        <div class="container">
          <div class="row">
            <div class="span12">
            <h3 style="padding: 40px;color: cadetblue;text-align:center">OUR CLIENTS </h3>
              <div id="owl-demo" class="owl-carousel">
                <div class="item"><img src="assets\images\clients\bank.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
                <div class="item"><img  src="assets\images\clients\cement.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
                <div class="item"><img  src="assets\images\clients\che.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
                <div class="item"><img  src="assets\images\clients\port.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
                <div class="item"><img  src="assets\images\clients\eng.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
                <div class="item"><img  src="assets\images\clients\doc.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
                <div class="item"><img  src="assets\images\clients\IT.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
				<div class="item"><img  src="assets\images\clients\PHARMACEUTICALS.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
				<div class="item"><img  src="assets\images\clients\steel.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
				<div class="item"><img  src="assets\images\clients\POWERINDUSTRY.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
				<div class="item"><img  src="assets\images\clients\FMCG.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
				<div class="item"><img  src="assets\images\clients\txt.jpg" alt="Owl Image">
                <h5>Employee</h5>
                </div>
              </div>
              
            </div>
          </div>
        </div>

    </div>
	
	<section class="mbr-section mbr-section-md-padding mbr-footer footer1" id="contacts1-1m" style="background-color: rgb(46, 46, 46); padding-top: 90px; padding-bottom: 90px;">
    
    <div class="container">
        <div class="row">
            <div class="mbr-footer-content col-xs-12 col-md-3">
                				<strong>Follow Us</strong>
					<div class="social">
						<ul>
							<li style="    display: inline-flex;"><a href="#" class="facebook"></a>&nbsp; 
							<a href="#" class="facebook twitter"> </a>&nbsp; 
							<a href="#" class="facebook chrome"> </a>&nbsp; 
							<a href="#" class="facebook in"> </a></li>
							
						</ul>		
						</div>
            </div>
            <div class="mbr-footer-content col-xs-12 col-md-3">
                <p class=""><strong>Address</strong><br>
1234 Street Name<br>
City, AA 99999</p>
            </div>
            <div class="mbr-footer-content col-xs-12 col-md-3">
                <p class=""><strong>Contacts</strong><br>
Email: support@mobirise.com<br>
Phone: +1 (0) 000 0000 001<br>
Fax: +1 (0) 000 0000 002</p>
            </div>
            <div class="mbr-footer-content col-xs-12 col-md-3">
                <p class=""><strong>Links</strong><br>
<a class="text-primary" href="https://mobirise.com/">Website builder</a><br>
<a class="text-primary" href="https://mobirise.com/mobirise-free-win.zip">Download for Windows</a><br>
<a class="text-primary" href="https://mobirise.com/mobirise-free-mac.zip">Download for Mac</a></p>
            </div>

        </div>
    </div>
</section>


   <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/SmoothScroll.js"></script>
  <script src="assets/viewportChecker/jquery.viewportchecker.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touchSwipe/jquery.touchSwipe.min.js"></script>
  <script src="assets/jarallax/jarallax.js"></script>
  <script src="assets/masonry/masonry.pkgd.min.js"></script>
  <script src="assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/pluginjs/owl.carousel.js"></script>
  <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        autoPlay: 3000,
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
      });

    });
    </script>
      	<script>
$(document).on('click', '.panel-heading span.clickable', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).on('click', '.panel div.clickable', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).ready(function () {
    $('.panel-heading span.clickable').click();
    $('.panel div.clickable').click();
});

	</script>