<section class="mbr-section article mbr-parallax-background mbr-after-navbar" id="msg-box8-11" style="background-image: url(assets/images/full-unsplash-photo-1460925895917-afdab827c52f-2000x1424-57.jpg); padding-top: 120px; padding-bottom: 120px;">

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(34, 34, 34);">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-xs-center">
                <h3 class="mbr-section-title display-2" style="color:white;">Blog</h3>
                <div class="lead"></div>
                
            </div>
        </div>
    </div>

</section>

<hr>

<div class="container">
		 <div class="content-grids">
			 <div class="col-md-8 content-main">
				 <div class="content-grid">					 
					 <div class="content-grid-info">
						 <img src="assets/images/post1.jpg" alt="" style="width: 100%;">
						 <div class="post-info">
						 <h4><a href="single.html" style="line-height:45px;color:cadetblue;">Lorem ipsum dolor sit amet</a>  July 30, 2014 </h4>
						 <p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis.</p>
						 <a href="single.html"><span></span>READ MORE</a>
						 </div>
					 </div>
					 <div class="content-grid-info">
						 <img src="assets/images/post2.jpg" alt="" style="width: 100%;">
						 <div class="post-info">
						 <h4><a href="single.html" style="line-height:45px;color:cadetblue;">Lorem ipsum dolor sit amet</a>  July 30, 2014  </h4>
						 <p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis.</p>
						 <a href="single.html"><span></span>READ MORE</a>
						 </div>
					 </div>
					 <div class="content-grid-info">
						 <img src="assets/images/post3.jpg" alt="" style="width: 100%;">
						 <div class="post-info">
						 <h4><a href="single.html" style="line-height:45px;color:cadetblue;">Lorem ipsum dolor sit amet</a>  July 30, 2014 </h4>
						 <p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis.</p>
						 <a href="single.html"><span></span>READ MORE</a>
						 </div>
					 </div>
					 
				 </div>
			  </div>
			  <div class="col-md-4 content-right">
				 <div class="recent">
					 <h3>RECENT POSTS</h3>
					 <ul>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">Aliquam tincidunt mauris</a></li>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">Vestibulum auctor dapibus  lipsum</a></li>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">Nunc dignissim risus consecu</a></li>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">Cras ornare tristiqu</a></li>
					 </ul>
				 </div>
			
				 <div class="clearfix"></div>
				 <div class="archives">
					 <h3>ARCHIVES</h3>
					 <ul>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">October 2013</a></li>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">September 2013</a></li>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">August 2013</a></li>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">July 2013</a></li>
					 </ul>
				 </div>
				 <div class="categories">
					 <h3>CATEGORIES</h3>
					 <ul>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">Vivamus vestibulum nulla</a></li>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">Integer vitae libero ac risus e</a></li>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">Vestibulum commo</a></li>
					 <li style="line-height:30px;"><a href="#" style="color:cadetblue;">Cras iaculis ultricies</a></li>
					 </ul>
				 </div>
				 <div class="clearfix"></div>
			  </div>
			  <div class="clearfix"></div>
		  </div>
	  </div>
<hr>
  <input name="animation" type="hidden">