package com.locusera.command;

import org.springframework.web.multipart.MultipartFile;

public class ResumeCommand {

	
	
	public MultipartFile resume;

	public MultipartFile getResume() {
		return resume;
	}

	public void setResume(MultipartFile resume) {
		this.resume = resume;
	}

	
	
}
