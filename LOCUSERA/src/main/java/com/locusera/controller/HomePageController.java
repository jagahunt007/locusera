package com.locusera.controller;

import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.locusera.command.ResumeCommand;
import com.locusera.utility.Constant;

@Controller
public class HomePageController {
	
	Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap("cloud_name",Constant.CLOUD_NAME,"api_key", Constant.API_KEY,"api_secret", Constant.API_SECRET_KEY));
	
	@RequestMapping(value="/index",method = RequestMethod.GET)
	public String home(Model model) {

	   model.addAttribute("resumeCommand", new ResumeCommand());
		return "LEhome";
	}
	@RequestMapping(value="/about",method = RequestMethod.GET)
	public String aboutus(Model model) {

	   model.addAttribute("about","active");
		return "LEabout";
	}
	@RequestMapping(value="/services",method = RequestMethod.GET)
	public String services(Model model) {

		   model.addAttribute("services","active");
		return "LEservices";
	}
	@RequestMapping(value="/jobseeker",method = RequestMethod.GET)
	public String jobseeker(Model model) {


		   model.addAttribute("jobseeker","active");

		return "LEjobseeker";
	}
	@RequestMapping(value="/enquiry",method = RequestMethod.GET)
	public String enquiry(Model model) {



		   model.addAttribute("enquiry","active");

		return "LEenquiry";
	}
	
	@RequestMapping(value="/blog",method = RequestMethod.GET)
	public String blog(Model model) {


		   model.addAttribute("bolg","active");
		return "LEblog";
	}
	
	@RequestMapping(value="/contactus",method = RequestMethod.GET)
	public String contactus(Model model) {


		   model.addAttribute("contact","active");
		return "LEcontact";
	}
	
	@RequestMapping("/submitResume")
	public String submitResume(ResumeCommand resumeCommand,HttpServletRequest req){
		
		
		MultipartFile file = resumeCommand.getResume();
		String resumeURL;
		   try{
		        byte[] l = null;

		     Random rand = new Random();
		     int imagerandom = rand.nextInt(9000000) + 1000000;
		          l = file.getBytes();


		          Map params = ObjectUtils.asMap("public_id", "carrierprofile/"+file.getOriginalFilename()+"-JH-"+imagerandom+""+"");
		           Map uploadResult = null;

		          uploadResult = cloudinary.uploader().upload(l, params);
		          
		          String d =  (String) uploadResult.get("url");
		          String pid =(String) uploadResult.get("public_id");
		          String format =(String) uploadResult.get("format");
		          resumeURL= pid+"."+format;
		             
		                 }
		                 
		                 catch(Exception ex)
		                 {
		                   ex.printStackTrace();
		                  
		                 }

		
		return "redirect:"+req.getHeader("referer");
	}
    
    
    
    
	

}
